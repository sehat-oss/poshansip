# Poshansip

Poshansip (Pos Hansip) is an indonesian words. The term Hansip come from the term "Pertahanan Sipil", which means civilian community security officer.
While "Pos Hansip" itself is a post that is used by Hansip, people/guests come to their posts to ask permission to visit their neighborhood. 
As it's name, poshansip act as a central auth server on sisehat environment

## How to run
Use go build command to build the application

```bash
go build main.go
```

and then run the application

```bash
poshansip
```

### Version

execute the command below to get current installed application

```bash
poshansip version
```

### Help

execute the command below to get some help

```bash
poshansip help
```


## Notes
If you are using Windows, please use poshansip.exe instead