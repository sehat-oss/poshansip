package token

// Options define functional options for token
type Options func(*Token)

// WithSecretKey assig secret key to token
func WithSecretKey(secretKey string) Options {
	return func(token *Token) {
		token.hmac = []byte(secretKey)
	}
}

// WithAccessTokenTTL assign access token ttl
func WithAccessTokenTTL(ttl uint64) Options {
	return func(token *Token) {
		token.ttl.accessToken = ttl
	}
}

// WithRefreshTokenTTL assign refresh token ttl
func WithRefreshTokenTTL(ttl uint64) Options {
	return func(token *Token) {
		token.ttl.refreshToken = ttl
	}
}
