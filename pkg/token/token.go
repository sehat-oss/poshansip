package token

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var (
	token  *Token
	doOnce = new(sync.Once)
)

// Token define struct for token
type Token struct {
	hmac []byte
	ttl  ttl
}

type ttl struct {
	accessToken  uint64
	refreshToken uint64
}

// Init initialized token
func Init(opts ...Options) {
	doOnce.Do(func() {
		token = new(Token)
	})

	for _, opt := range opts {
		opt(token)
	}
}

// GetImplicitToken generate new access token for implicit grant
func GetImplicitToken(payload Payload) (tokenStr string, expiresAt int64, err error) {
	tokenStr, expiresAt, err = createToken(payload, token.ttl.refreshToken)
	return
}

// GetNewToken generate new access token
func GetNewToken(payload Payload, refreshToken string) (tokenStr string, expiresAt int64, err error) {
	valid, err := IsTokenValid(refreshToken)
	if err != nil {
		return
	}

	if !valid {
		err = errors.New("Refresh token is not valid")
		return
	}

	tokenStr, expiresAt, err = createToken(payload, token.ttl.accessToken)
	return
}

// GetRefreshToken generate new refresh token
func GetRefreshToken(payload Payload) (tokenStr string, expiresAt int64, err error) {
	tokenStr, expiresAt, err = createToken(payload, token.ttl.refreshToken)
	return
}

func createToken(payload Payload, ttl uint64) (tokenStr string, expiresAt int64, err error) {

	jwtStandardClaims := jwt.StandardClaims{
		IssuedAt: time.Now().Unix(),
		Issuer:   issuer,
	}

	if ttl > 0 {
		expiresAt = time.Now().Add(time.Duration(ttl) * time.Minute).Unix()
		jwtStandardClaims.ExpiresAt = expiresAt
	}

	claims := Claims{
		Payload{
			UserID: payload.UserID,
		},
		jwtStandardClaims,
	}

	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err = jwtToken.SignedString(token.hmac)
	return
}

// IsTokenValid check whether token still valid
func IsTokenValid(tokenString string) (valid bool, err error) {
	token, err := jwt.Parse(tokenString, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", jwtToken.Header["alg"])
		}

		return token.hmac, nil
	})

	if err != nil {
		return
	}

	valid = token.Valid
	return
}

// GetClaims get token claims
func GetClaims(tokenString string) (claims jwt.MapClaims, err error) {
	token, err := jwt.Parse(tokenString, func(jwtToken *jwt.Token) (interface{}, error) {
		if _, ok := jwtToken.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", jwtToken.Header["alg"])
		}

		return token.hmac, nil
	})

	if err != nil {
		return
	}

	claims = token.Claims.(jwt.MapClaims)

	return
}
