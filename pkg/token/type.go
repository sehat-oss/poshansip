package token

import "github.com/dgrijalva/jwt-go"

// Payload define payload body for token
type Payload struct {
	UserID uint64
}

// Claims define custom jwt claims
type Claims struct {
	Payload
	jwt.StandardClaims
}

const (
	issuer = "poshansip.sehat.app"
)

const (
	// UserIDClaims const for user id payload field
	UserIDClaims = "UserID"
)
