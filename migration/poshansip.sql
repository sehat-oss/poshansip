CREATE DATABASE poshansip;

USE poshansip;

CREATE SEQUENCE client_table_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE clients (
	id INT8 NOT NULL DEFAULT nextval('client_table_seq':::STRING),
	client_id VARCHAR NOT NULL,
	client_secret VARCHAR NOT NULL,
	client_name VARCHAR NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	updated_at TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX clients_client_id_idx (client_id ASC, client_secret ASC),
	FAMILY "primary" (id, client_id, client_secret, client_name, created_at, updated_at)
);

CREATE SEQUENCE users_table_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE users (
	id INT8 NOT NULL DEFAULT nextval('users_table_seq':::STRING),
	username VARCHAR NOT NULL,
	password VARCHAR NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	updated_at TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	FAMILY "primary" (id, username, password, created_at, updated_at)
);

CREATE SEQUENCE tokens_table_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE tokens (
	id INT8 NOT NULL DEFAULT nextval('tokens_table_seq':::STRING),
	user_id INT8 NOT NULL,
	access_token VARCHAR(250) NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	refresh_token VARCHAR(250) NOT NULL,
	access_token_expires TIMESTAMP NULL,
	refresh_token_expires TIMESTAMP NULL,
	grant_type VARCHAR NOT NULL,
	client_id INT8 NOT NULL,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX tokens_auto_index_tokens_fk (user_id ASC),
	INDEX tokens_auto_index_clients_fk (client_id ASC),
	FAMILY "primary" (id, user_id, access_token, created_at, refresh_token, access_token_expires, refresh_token_expires, grant_type, client_id)
);

CREATE SEQUENCE users_clients_table_seq MINVALUE 1 MAXVALUE 9223372036854775807 INCREMENT 1 START 1;

CREATE TABLE users_clients (
	id INT8 NOT NULL DEFAULT nextval('users_clients_table_seq':::STRING),
	client_id INT8 NOT NULL,
	user_id INT8 NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	updated_at TIMESTAMP NOT NULL DEFAULT now():::TIMESTAMP,
	CONSTRAINT "primary" PRIMARY KEY (id ASC),
	INDEX users_clients_user_id_idx (user_id ASC),
	INDEX users_clients_client_id_idx (client_id ASC),
	FAMILY "primary" (id, client_id, user_id, created_at, updated_at)
);

SELECT setval('client_table_seq', 4, false);

INSERT INTO clients (id, client_id, client_secret, client_name, created_at, updated_at) VALUES
	(1, '9304', 'i0llf04kl', 'klinik', '2019-11-19 14:36:02.216461+00:00', '2019-11-19 14:36:02.216461+00:00'),
	(2, '8078', 'o3k{9ko', 'pasien', '2019-11-19 14:36:46.271106+00:00', '2019-11-19 14:36:46.271106+00:00'),
	(3, '2804', 'k89K|{9k', 'dokter', '2019-11-19 14:37:52.471605+00:00', '2019-11-19 14:37:52.471605+00:00');

SELECT setval('users_table_seq', 2, false);

INSERT INTO users (id, username, password, created_at, updated_at) VALUES
	(1, 'raspiantoro@gmail.com', 'pass', '2019-10-19 15:25:21.747046+00:00', '2019-10-19 15:25:21.747046+00:00');

SELECT setval('tokens_table_seq', 1, false);

SELECT setval('users_clients_table_seq', 3, false);

INSERT INTO users_clients (id, client_id, user_id, created_at, updated_at) VALUES
	(1, 1, 1, '2019-11-19 14:38:23.925577+00:00', '2019-11-19 14:38:23.925577+00:00'),
	(2, 2, 1, '2019-11-19 14:38:26.379282+00:00', '2019-11-19 14:38:26.379282+00:00');

ALTER TABLE tokens ADD CONSTRAINT tokens_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE tokens ADD CONSTRAINT clients_fk FOREIGN KEY (client_id) REFERENCES clients (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE users_clients ADD CONSTRAINT users_clients_fk FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE users_clients ADD CONSTRAINT users_clients_clientid_fk FOREIGN KEY (client_id) REFERENCES clients (id) ON DELETE CASCADE ON UPDATE CASCADE;

-- Validate foreign key constraints. These can fail if there was unvalidated data during the dump.
ALTER TABLE tokens VALIDATE CONSTRAINT tokens_fk;
ALTER TABLE tokens VALIDATE CONSTRAINT clients_fk;
ALTER TABLE users_clients VALIDATE CONSTRAINT users_clients_fk;
ALTER TABLE users_clients VALIDATE CONSTRAINT users_clients_clientid_fk;
