module gitlab.com/sehat-oss/poshansip

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.2.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.3.2
	gitlab.com/sehat-oss/poshansip-go-proto v0.0.0-20191117090747-d7ba972bce74
	google.golang.org/grpc v1.24.0
)
