package app

import (
	clientRepository "gitlab.com/sehat-oss/poshansip/internal/client/repositories"
	tokenRepository "gitlab.com/sehat-oss/poshansip/internal/token/repositories"
	userRepository "gitlab.com/sehat-oss/poshansip/internal/user/repositories"
)

// InitRepositories init application repositories
func InitRepositories(app IApplication) {
	dbM := app.GetDatabaseManager()
	repo := app.GetRepositories()

	repo.user = userRepository.New(
		userRepository.WithDatabaseManager(dbM),
	)

	repo.token = tokenRepository.New(
		tokenRepository.WithDatabaseManager(dbM),
	)

	repo.client = clientRepository.New(
		clientRepository.WithDatabaseManager(dbM),
	)

}
