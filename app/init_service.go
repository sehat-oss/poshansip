package app

import (
	authSvc "gitlab.com/sehat-oss/poshansip/internal/auth/services"
	clientSvc "gitlab.com/sehat-oss/poshansip/internal/client/services"
	tokenSvc "gitlab.com/sehat-oss/poshansip/internal/token/services"
	userSvc "gitlab.com/sehat-oss/poshansip/internal/user/services"
)

// InitServices init application services
func InitServices(app IApplication) {
	svc := app.GetServices()
	repo := app.GetRepositories()

	svc.auth = authSvc.New(
		authSvc.WithUserRepository(repo.user),
		authSvc.WithTokenRepository(repo.token),
	)

	svc.user = userSvc.New(
		userSvc.WithUserRepository(repo.user),
	)

	svc.token = tokenSvc.New(
		tokenSvc.WithTokenRepository(repo.token),
	)

	svc.client = clientSvc.New(
		clientSvc.WithClientRepository(repo.client),
	)
}
