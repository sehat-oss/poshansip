package app

import (
	"gitlab.com/sehat-oss/poshansip/driver/db"
)

// InitDatabase initialize application databases
func InitDatabase(app IApplication) (err error) {
	cfg := app.GetConfig()
	dbM := app.GetDatabaseManager()

	err = dbM.Init(
		db.WithPoshansipDB(cfg.PoshansipDB),
	)

	return
}
