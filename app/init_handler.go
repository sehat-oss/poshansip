package app

import (
	protohansip "gitlab.com/sehat-oss/poshansip-go-proto"
	authH2 "gitlab.com/sehat-oss/poshansip/internal/auth/handler/grpc"
)

// InitHandler init application handlers
func InitHandler(app IApplication) {
	initGrpc(app)
}

func initGrpc(app IApplication) {
	server := app.GetServers().grpc
	grpcHandler := app.GetHandlers().grpc

	grpcHandler.auth = authH2.New(
		authH2.WithAuthService(app.GetServices().auth),
		authH2.WithUserService(app.GetServices().user),
		authH2.WithTokenService(app.GetServices().token),
		authH2.WithClientService(app.GetServices().client),
	)

	protohansip.RegisterAuthServer(server, grpcHandler.auth)
}
