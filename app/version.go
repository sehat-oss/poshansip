package app

import (
	"fmt"

	"gitlab.com/sehat-oss/poshansip/version"
)

//GetVersion get current version of the application
func GetVersion() {
	fmt.Println("Poshansip version   :", version.Number)

	if version.Environment != "production" {
		fmt.Println("Build from commit   :", version.GitCommit)
		fmt.Println("Environment         :", version.Environment)
	}

	fmt.Println("Build on            :", version.BuildDate)
	fmt.Println("Go version          :", version.GoVersion)
	fmt.Println("Os architecture     :", version.OsArch)
}
