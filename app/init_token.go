package app

import (
	"gitlab.com/sehat-oss/poshansip/config"
	"gitlab.com/sehat-oss/poshansip/pkg/token"
)

// InitToken initialize global token instance
func InitToken(cfg *config.Config) {
	token.Init(
		token.WithSecretKey(cfg.Token.Secret),
		token.WithAccessTokenTTL(cfg.Token.TTL.AccessToken),
		token.WithRefreshTokenTTL(cfg.Token.TTL.RefreshToken),
	)
}
