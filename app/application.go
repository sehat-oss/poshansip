package app

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/sehat-oss/poshansip/internal/client"

	"gitlab.com/sehat-oss/poshansip/internal/token"

	protohansip "gitlab.com/sehat-oss/poshansip-go-proto"
	"gitlab.com/sehat-oss/poshansip/config"
	"gitlab.com/sehat-oss/poshansip/driver/db"
	"gitlab.com/sehat-oss/poshansip/internal/auth"
	"gitlab.com/sehat-oss/poshansip/internal/user"
	"google.golang.org/grpc"
)

// IApplication define application interface
type IApplication interface {
	Init()
	GetServers() *server
	GetHandlers() *handler
	GetServices() *service
	GetRepositories() *repository
	GetConfig() *config.Config
	GetDatabaseManager() *db.Manager
}

type application struct {
	servers      *server
	handlers     *handler
	services     *service
	repositories *repository
	dbM          *db.Manager
	appSignal    chan os.Signal
	listener     net.Listener
	config       *config.Config
}

type server struct {
	grpc *grpc.Server
}

type handler struct {
	grpc grpcHandler
}

type grpcHandler struct {
	auth protohansip.AuthServer
}

type service struct {
	auth   auth.Service
	user   user.Service
	token  token.Service
	client client.Service
}

type repository struct {
	user   user.Repository
	token  token.Repository
	client client.Repository
}

// GetApplication get application instance
func GetApplication() IApplication {
	return new(application)
}

// GetServers get servers instance
func (a *application) GetServers() *server {
	if a.servers == nil {
		a.servers = new(server)
	}
	return a.servers
}

// GetHandlers get handler instance
func (a *application) GetHandlers() *handler {
	if a.handlers == nil {
		a.handlers = new(handler)
	}
	return a.handlers
}

// GetServices get service instance
func (a *application) GetServices() *service {
	if a.services == nil {
		a.services = new(service)
	}
	return a.services
}

// GetRepositories get repository instance
func (a *application) GetRepositories() *repository {
	if a.repositories == nil {
		a.repositories = new(repository)
	}
	return a.repositories
}

// GetConfig get application configs
func (a *application) GetConfig() *config.Config {
	return a.config
}

// GetDatabaseManager get application database manager
func (a *application) GetDatabaseManager() *db.Manager {
	if a.dbM == nil {
		a.dbM = new(db.Manager)
	}
	return a.dbM
}

// Init init the application
func (a *application) Init() {
	var err error
	//Assign the signal channel to stop the current server
	a.appSignal = make(chan os.Signal, 1)
	signal.Notify(a.appSignal, os.Interrupt, os.Kill, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)

	log.Println("Initialize app configs is in porcess")
	a.config, err = InitConfig()
	if err != nil {
		log.Fatalf("Couldn't load config: %s", err)
	}
	log.Println("App configs has been initialized")

	InitToken(a.config)
	log.Println("Initialize app database is in porcess")
	err = InitDatabase(a)
	if err != nil {
		log.Fatalf("Couldn't connect to database: %s", err)
	}
	log.Println("App database has been initialized")

	log.Println("Initialize repositories is in porcess")
	InitRepositories(a)
	log.Println("Repositories has been initialized")

	log.Println("Initialize services is in porcess")
	InitServices(a)
	log.Println("Services has been initialized")

	log.Println("Initialize grpc server is in porcess")
	InitGrpc(a)
	log.Println("Grpc server has been initialized")

	log.Println("Initialize handlers is in porcess")
	InitHandler(a)
	log.Println("Handlers has been initialized")

	a.run()
}

func (a *application) run() {
	var err error
	// create a listener on TCP port 7777
	a.listener, err = net.Listen("tcp", fmt.Sprintf(":%d", 7777))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	go a.runGrpc(a.listener)
	defer a.Close()

	log.Println("Server start on ")
	log.Println("GRPC: localhost:7777")

	<-a.appSignal
}

func (a *application) runGrpc(lis net.Listener) {
	var (
		err error
	)

	// start the server
	err = a.servers.grpc.Serve(lis)
	if err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}

// Close close all resources regarding to this service.
func (a *application) Close() (err error) {
	close(a.appSignal)
	a.servers.grpc.Stop()
	a.listener.Close()
	log.Println("Bye, poshansip has been stoped!")
	return
}
