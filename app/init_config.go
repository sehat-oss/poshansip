package app

import "gitlab.com/sehat-oss/poshansip/config"

// InitConfig init application configs
func InitConfig() (cfg *config.Config, err error) {
	cfg, err = config.GetConfig("config", ".")
	return
}
