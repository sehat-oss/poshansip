package app

import "google.golang.org/grpc"

// InitGrpc instantiate grpc server
func InitGrpc(a IApplication) {
	server := a.GetServers()
	server.grpc = grpc.NewServer()
}
