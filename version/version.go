package version

import (
	"fmt"
	"runtime"
)

var (
	//Number the number of version that are running at the moment.
	Number string

	//GitCommit compiled from which commit.
	GitCommit string

	//Environment the environment that is used by this version.
	Environment string

	//BuildDate date of the application was build.
	BuildDate = ""

	//GoVersion version of go.
	GoVersion = runtime.Version()

	//OsArch os architecture that used to run this application.
	OsArch = fmt.Sprintf("%s %s", runtime.GOOS, runtime.GOARCH)
)
