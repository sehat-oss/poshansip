.PHONY: default help install build build-windows run run-windows

APP_NAME     = poshansip
VERSION      ?= $(shell git describe --always --tags)
GIT_COMMIT   ?= $(shell git rev-parse --short HEAD)
GIT_BRANCH   ?= $(shell git rev-parse --abbrev-ref HEAD)
ENVIRONMENT  ?= $(shell ([[ $(GIT_BRANCH) == 'master' ]] && echo "production") || ([[ $(GIT_BRANCH) == 'staging' ]] && echo "staging") || echo "development")
BUILD_DATE   = $(shell date '+%Y-%m-%d-%H:%M:%S')

default: help

help:
	@echo 'These are common ${APP_NAME} commands used in varios situations:'
	@echo
	@echo 'Usage:'
	@echo '   make install             Install all project dependencies.'
	@echo '   make build               Build the project.'
	@echo '   make build-windows       Build the project for Windows.'
	@echo '   make run ARGS=           Running the application with supplied arguments.'
	@echo '   make run-windows ARGS=   Running the application supplied arguments on Windows.'
	@echo '   make package             Build final Docker image with just the Go binary inside.'
	@echo '   make tag                 Tag image created by package with latest, git commit and version.'
	@echo '   make docker-run          Running the application with docker.'
	@echo '   make docker-run-build    Running the application with docker (build first).'
	@echo '   make docker-stop         Stop docker container.'
	@echo '   make docker-rm           Remove docker container.'
	@echo '   make compose-up          Running the application with docker-compose (inc database and others).'
	@echo '   make compose-up-build    Running the application with docker-compose (inc database and others, build first).'
	@echo '   make compose-stop        Stop running container on docker-compose.'
	@echo '   make compose-down        Stop and remove running container on docker-compose.'
	@echo '   make prepare-compose     Prepare for docker-compose.'
	@echo '   make migrate-up          Migrate poshansip database.'
	@echo '   make test                Run tests on a compiled project.'
	@echo '   make clean               Clean the directory tree.'

install:
	@echo "Install project dependencies"
	go get -v ./...

build: install
	@echo "Building ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	go build -ldflags "-X gitlab.com/sehat-oss/poshansip/version.Number=${VERSION} -X gitlab.com/sehat-oss/poshansip/version.GitCommit=${GIT_COMMIT} -X gitlab.com/sehat-oss/poshansip/version.Environment=${ENVIRONMENT} -X gitlab.com/sehat-oss/poshansip/version.BuildDate=${BUILD_DATE}" -o bin/${APP_NAME}

build-windows: install
	@echo "Building ${APP_NAME}.exe $(VERSION) $(GIT_COMMIT)"
	go build -ldflags "-X gitlab.com/sehat-oss/poshansip/version.Number=${VERSION} -X gitlab.com/sehat-oss/poshansip/version.GitCommit=${GIT_COMMIT} -X gitlab.com/sehat-oss/poshansip/version.Environment=${ENVIRONMENT} -X gitlab.com/sehat-oss/poshansip/version.BuildDate=${BUILD_DATE}" -o bin/${APP_NAME}.exe

run: build
	@echo "Running ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	bin/${APP_NAME} ${ARGS}

run-windows: build-windows
	@echo "Running ${APP_NAME}.exe $(VERSION) $(GIT_COMMIT)"
	bin/${APP_NAME}.exe ${ARGS}

package:
	@echo "Building image ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	docker build --build-arg VERSION=$(VERSION) --build-arg GIT_COMMIT=$(GIT_COMMIT) --build-arg ENVIRONMENT=$(ENVIRONMENT) --build-arg GOPROXY=${GOPROXY} -t ${APP_NAME}:local .

tag: package
	@echo "Tagging: latest $(VERSION) $(GIT_COMMIT)"
	docker tag ${APP_NAME}:local ${APP_NAME}:$(GIT_COMMIT)
	docker tag ${APP_NAME}:local ${APP_NAME}:$(VERSION)
	docker tag ${APP_NAME}:local ${APP_NAME}:latest

docker-run:
	@echo "Running with docker image ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	docker run -p 7777:7777 --name poshansip poshansip:latest

docker-run-build: tag
	@echo "Running with docker image ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	docker run -p 7777:7777 --name poshansip poshansip:latest

docker-stop:
	@echo "Stop docker container ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	docker stop poshansip

docker-rm: docker-stop
	@echo "Remove docker container ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	docker rm poshansip

compose-up:
	@echo "Running with docker compose ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	docker-compose up -d

compose-up-build: tag
	@echo "Running with docker compose ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	docker-compose up -d

compose-stop:
	@echo "Stop docker compose ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	docker-compose stop

compose-down:
	@echo "Stop and remove docker compose ${APP_NAME} $(VERSION) $(GIT_COMMIT)"
	docker-compose down

prepare-compose:
	@echo "Creating docker network"
	docker network create sehatnet
	@echo "Creating docker volume"
	docker volume create data-volume1
	docker volume create data-volume2
	docker volume create data-volume3

migrate-up:
	@echo "Start poshansip db migration"
	docker-compose exec roach1 sh -c "./cockroach sql --insecure < poshansip.sql"
	@echo "Finish migrating poshansip db"

test:
	@echo "Testing ${APP_NAME} ${VERSION}"
	go test ./...

clean:
	@echo "Removing ${APP_NAME} ${VERSION}"
	@test ! -e bin/${APP_NAME} || rm bin/${APP_NAME}