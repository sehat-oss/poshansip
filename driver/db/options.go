package db

import (
	"fmt"

	"github.com/jmoiron/sqlx"

	// used to initialized postgre db
	_ "github.com/lib/pq"
	"gitlab.com/sehat-oss/poshansip/config"
)

// Options define options for database manager
type Options func(*Manager) (err error)

// WithPoshansipDB assign poshansip database into database manager
func WithPoshansipDB(cfg config.PoshansipDatabase) Options {
	return func(m *Manager) (err error) {
		addr := fmt.Sprintf("postgresql://%s:%s@%s:%d/%s?ssl=%t&sslmode=%s",
			cfg.Username,
			cfg.Password,
			cfg.Hostname,
			cfg.Port,
			cfg.DbName,
			cfg.Ssl,
			cfg.SSLMod,
		)

		m.poshansip, err = sqlx.Connect("postgres", addr)
		if err != nil {
			return
		}

		return
	}
}
