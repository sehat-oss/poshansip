package db

import "github.com/jmoiron/sqlx"

// Manager define struct for database manager
type Manager struct {
	poshansip *sqlx.DB
}

// Init initialize database manager
func (m *Manager) Init(opts ...Options) (err error) {
	for _, opt := range opts {
		err = opt(m)
		if err != nil {
			return
		}
	}
	return
}

// GetPoshansip get poshansip database instance
func (m *Manager) GetPoshansip() *sqlx.DB {
	return m.poshansip
}
