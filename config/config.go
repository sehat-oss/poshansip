package config

import (
	"github.com/spf13/viper"
)

// Config define struct for apps config
type Config struct {
	PoshansipDB PoshansipDatabase `mapstructure:"poshansip_db"`
	Token       Token             `mapstructure:"token"`
}

// PoshansipDatabase define struct for poshansip database
type PoshansipDatabase struct {
	Hostname string `mapstructure:"hostname"`
	Port     uint64 `mapstructure:"port"`
	DbName   string `mapstructure:"dbname"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
	Ssl      bool   `mapstructure:"ssl"`
	SSLMod   string `mapstructure:"ssl_mod"`
}

// Token define struct for jwt config
type Token struct {
	Secret string   `mapstructure:"secret"`
	TTL    TokenTTL `mapstructute:"token.ttl"`
}

// TokenTTL define struct for token ttl config
type TokenTTL struct {
	AccessToken  uint64 `mapstructure:"access_token"`
	RefreshToken uint64 `mapstructure:"refresh_token"`
}

// GetConfig get application config
func GetConfig(configName string, configPath string) (config *Config, err error) {
	v := viper.New()
	v.SetConfigName(configName)
	v.AddConfigPath(configPath)

	if err = v.ReadInConfig(); err != nil {
		return
	}

	if err = v.Unmarshal(&config); err != nil {
		return
	}

	return
}
