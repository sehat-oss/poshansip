FROM golang:1.12-alpine3.10 AS build-stage

ARG ENVIRONMENT
ARG GOPROXY

LABEL maintainer="Mario Raspiantoro<raspiantoro@gmail.com>"
LABEL REPO="https://gitlab.com/sehat-oss/poshansip"

RUN apk add git && apk add --update make

RUN git config \
  --global url."https://gitlab-ci-token:3Uaq8BDhz3skJ6H4aYo_@gitlab.private/group".insteadOf \
  "git@gitlab.private:group"

WORKDIR /opt/poshansip

COPY . .

RUN make build


FROM alpine

LABEL maintainer="Mario Raspiantoro<raspiantoro@gmail.com>"
LABEL REPO="https://gitlab.com/sehat-oss/poshansip"

RUN apk update \
        && apk upgrade \
        && apk add --no-cache \
        ca-certificates \
        && update-ca-certificates 2>/dev/null || true

COPY --from=build-stage /opt/poshansip/bin/poshansip /opt/poshansip/bin/

WORKDIR /opt/poshansip/bin

RUN adduser -D -S poshansip
USER poshansip

CMD ["/opt/poshansip/bin/poshansip"]