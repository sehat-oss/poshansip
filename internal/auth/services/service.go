package services

import (
	"context"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/sehat-oss/poshansip/internal/token"

	"gitlab.com/sehat-oss/poshansip/internal/auth"
	"gitlab.com/sehat-oss/poshansip/internal/customerror"
	"gitlab.com/sehat-oss/poshansip/internal/user"
	userModels "gitlab.com/sehat-oss/poshansip/internal/user/models"
	tokenPkg "gitlab.com/sehat-oss/poshansip/pkg/token"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Service define struct for auth service
type Service struct {
	repository repository
}

type repository struct {
	user  user.Repository
	token token.Repository
}

// New create new instance of auth service
func New(opts ...Options) auth.Service {
	serv := new(Service)

	for _, opt := range opts {
		opt(serv)
	}

	return serv
}

// EmailAuthentication authenticate using email password
func (s *Service) EmailAuthentication(ctx context.Context, email string, password string) (user userModels.User, err error) {
	user, err = s.repository.user.FindByUsername(ctx, email)
	if err != nil {
		return
	}

	valid := s.isPasswordMatch(user.Password, password)
	if !valid {
		err = customerror.ErrAuthBadUsernamePassword
	}

	return
}

// RefreshTokenAuthentication authenticate using refresh token
func (s *Service) RefreshTokenAuthentication(ctx context.Context, token string) (claims jwt.MapClaims, err error) {
	valid, err := tokenPkg.IsTokenValid(token)
	if err != nil {
		return
	}

	if !valid {
		err = customerror.ErrAuthInvalidToken
		return
	}

	claims, err = tokenPkg.GetClaims(token)
	if err != nil {
		err = status.Error(codes.Internal, err.Error())
		return
	}

	return
}

// IsPasswordMatch compare password
func (s *Service) isPasswordMatch(storedPassword string, providedPassword string) (isMatch bool) {
	if storedPassword == providedPassword {
		isMatch = true
		return
	}

	return
}

// Logout define service for login
func (s *Service) Logout(ctx context.Context) (err error) {
	return
}
