package grpc

import (
	"context"
	"database/sql"
	"log"

	"gitlab.com/sehat-oss/poshansip/internal/client"

	"gitlab.com/sehat-oss/poshansip/internal/token"
	"gitlab.com/sehat-oss/poshansip/internal/user"

	protohansip "gitlab.com/sehat-oss/poshansip-go-proto"
	"gitlab.com/sehat-oss/poshansip/internal/auth"
	"gitlab.com/sehat-oss/poshansip/internal/customerror"
	tokenPkg "gitlab.com/sehat-oss/poshansip/pkg/token"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Handler grpc handler for auth domain
type Handler struct {
	services service
}

type service struct {
	auth   auth.Service
	user   user.Service
	token  token.Service
	client client.Service
}

// New create new instance of auth grpc handler
func New(opts ...Options) protohansip.AuthServer {
	handler := new(Handler)

	for _, opt := range opts {
		opt(handler)
	}

	return handler
}

// Login grpc handler for login
func (h2 *Handler) Login(ctx context.Context, req *protohansip.AuthenticationRequest) (resp *protohansip.AuthenticatitonResponse, err error) {

	if &req.GrantType == nil || req.GrantType == "" {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrAuthNoGrantType)
		err = status.Error(codes.InvalidArgument, customerror.ErrAuthNoGrantType.Error())
		return
	}

	if &req.ClientId == nil || req.ClientId == "" {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrNoClientIDProvided)
		err = status.Error(codes.InvalidArgument, customerror.ErrNoClientIDProvided.Error())
		return
	}

	if &req.ClientSecret == nil || req.ClientSecret == "" {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrNoClientSecretProvided)
		err = status.Error(codes.InvalidArgument, customerror.ErrNoClientSecretProvided.Error())
		return
	}

	if req.GrantType != auth.PasswordGrantType &&
		req.GrantType != auth.ImplicitGrantType &&
		req.GrantType != auth.RefreshTokenGrantType {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrAuthNotSupportedGrantType)
		err = status.Error(codes.Unimplemented, customerror.ErrAuthNotSupportedGrantType.Error())
		return
	}

	if req.GetGrantType() == auth.PasswordGrantType {
		resp, err = h2.handlePasswordGrant(ctx, req)
	}

	if req.GetGrantType() == auth.ImplicitGrantType {
		resp, err = h2.handleImplicitGrant(ctx, req)
	}

	if err == sql.ErrNoRows {
		log.Printf(auth.AuthErrorPrefix, err)
		err = status.Error(codes.NotFound, err.Error())
		return
	}

	if err == customerror.ErrAuthBadUsernamePassword {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrAuthBadUsernamePassword)
		err = status.Error(codes.NotFound, customerror.ErrAuthBadUsernamePassword.Error())
		return
	}

	if err == customerror.ErrWrongClientIDOrClientSecret {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrWrongClientIDOrClientSecret)
		err = status.Error(codes.InvalidArgument, customerror.ErrWrongClientIDOrClientSecret.Error())
		return
	}

	if err != nil {
		log.Printf(auth.AuthErrorPrefix, err)
		err = status.Error(codes.Internal, err.Error())
		return
	}

	if req.GetGrantType() == auth.RefreshTokenGrantType {
		resp, err = h2.handleRefreshTokenGrant(ctx, req)
	}

	if err == customerror.ErrAuthInvalidToken {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrAuthInvalidToken)
		err = status.Error(codes.Unauthenticated, customerror.ErrAuthInvalidToken.Error())
		return
	}

	if err == customerror.ErrAuthTokenHasBeenRevoked {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrAuthTokenHasBeenRevoked)
		err = status.Error(codes.Unauthenticated, customerror.ErrAuthTokenHasBeenRevoked.Error())
		return
	}

	if err == customerror.ErrAuthExtractTokenClaims {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrAuthExtractTokenClaims)
		err = status.Error(codes.Internal, customerror.ErrAuthExtractTokenClaims.Error())
		return
	}

	if err == customerror.ErrWrongClientIDOrClientSecret {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrWrongClientIDOrClientSecret)
		err = status.Error(codes.InvalidArgument, customerror.ErrWrongClientIDOrClientSecret.Error())
		return
	}

	if err != nil {
		log.Printf(auth.AuthErrorPrefix, err)
		err = status.Error(codes.Internal, err.Error())
		return
	}

	return
}

func (h2 *Handler) handlePasswordGrant(ctx context.Context, req *protohansip.AuthenticationRequest) (resp *protohansip.AuthenticatitonResponse, err error) {
	user, err := h2.services.auth.EmailAuthentication(ctx, req.Email, req.Password)
	if err != nil {
		return
	}

	valid, clientID, err := h2.services.client.ValidateUserClient(ctx, user.ID, req.ClientId, req.ClientSecret)
	if err != nil {
		return
	}
	if !valid {
		err = customerror.ErrWrongClientIDOrClientSecret
		return
	}

	token, err := h2.services.token.GetPasswordGrantToken(ctx, user, clientID)
	if err != nil {
		return
	}

	resp = &protohansip.AuthenticatitonResponse{
		Token:        token.AccessToken,
		RefreshToken: token.RefreshToken,
	}

	return
}

func (h2 *Handler) handleImplicitGrant(ctx context.Context, req *protohansip.AuthenticationRequest) (resp *protohansip.AuthenticatitonResponse, err error) {
	user, err := h2.services.auth.EmailAuthentication(ctx, req.Email, req.Password)
	if err != nil {
		return
	}

	valid, clientID, err := h2.services.client.ValidateUserClient(ctx, user.ID, req.ClientId, req.ClientSecret)
	if err != nil {
		return
	}
	if !valid {
		err = customerror.ErrWrongClientIDOrClientSecret
		return
	}

	token, err := h2.services.token.GetImplicitGrantToken(ctx, user, clientID)
	if err != nil {
		return
	}

	resp = &protohansip.AuthenticatitonResponse{
		Token: token.AccessToken,
	}

	return
}

func (h2 *Handler) handleRefreshTokenGrant(ctx context.Context, req *protohansip.AuthenticationRequest) (resp *protohansip.AuthenticatitonResponse, err error) {
	claims, err := h2.services.auth.RefreshTokenAuthentication(ctx, req.RefreshToken)
	if err != nil {
		return
	}

	userIDClaims, ok := claims[tokenPkg.UserIDClaims]
	if !ok {
		err = customerror.ErrAuthExtractTokenClaims
		return
	}

	userID := uint64(userIDClaims.(float64))

	valid, clientID, err := h2.services.client.ValidateUserClient(ctx, userID, req.ClientId, req.ClientSecret)
	if err != nil {
		return
	}

	if !valid {
		err = customerror.ErrWrongClientIDOrClientSecret
		return
	}

	isExistOnDB, err := h2.services.token.CheckRefreshTokenOnDB(ctx, req.RefreshToken, userID)
	if err != nil {
		return
	}

	if !isExistOnDB {
		err = customerror.ErrAuthTokenHasBeenRevoked
		return
	}

	token, err := h2.services.token.GetRefreshTokenGrantToken(ctx, req.RefreshToken, userID, clientID)
	if err != nil {
		return
	}

	resp = &protohansip.AuthenticatitonResponse{
		Token: token.AccessToken,
	}

	return
}

// Logout grpc handler for logout
func (h2 *Handler) Logout(ctx context.Context, req *protohansip.LogoutRequest) (resp *protohansip.LogoutResponse, err error) {

	if req.Token == "" || &req.Token == nil {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrNoTokenProvided)
		err = status.Error(codes.InvalidArgument, customerror.ErrNoTokenProvided.Error())
		return
	}

	claims, err := h2.services.auth.RefreshTokenAuthentication(ctx, req.Token)
	if err == customerror.ErrAuthInvalidToken {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrAuthInvalidToken)
		err = status.Error(codes.Unauthenticated, customerror.ErrAuthInvalidToken.Error())
		return
	}
	if err == customerror.ErrAuthExtractTokenClaims {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrAuthExtractTokenClaims)
		err = status.Error(codes.Internal, customerror.ErrAuthExtractTokenClaims.Error())
		return
	}
	if err != nil {
		log.Printf(auth.AuthErrorPrefix, err)
		err = status.Error(codes.Internal, err.Error())
		return
	}

	userIDClaims, ok := claims[tokenPkg.UserIDClaims]
	if !ok {
		err = customerror.ErrAuthExtractTokenClaims
		return
	}

	userID := uint64(userIDClaims.(float64))

	isExistOnDB, token, err := h2.services.token.CheckAccessTokenOnDB(ctx, req.Token, userID)
	if !isExistOnDB {
		log.Printf(auth.AuthErrorPrefix, customerror.ErrAuthTokenHasBeenRevoked)
		err = status.Error(codes.Unauthenticated, customerror.ErrAuthTokenHasBeenRevoked.Error())
		return
	}
	if err != nil {
		log.Printf(auth.AuthErrorPrefix, err)
		err = status.Error(codes.Internal, err.Error())
		return
	}

	err = h2.services.token.RevokeToken(ctx, token.ID)
	if err != nil {
		log.Printf(auth.AuthErrorPrefix, err)
		err = status.Error(codes.Internal, err.Error())
		return
	}

	resp = &protohansip.LogoutResponse{
		Message: "Logout berhasil",
	}

	return
}
