package grpc

import (
	"gitlab.com/sehat-oss/poshansip/internal/auth"
	"gitlab.com/sehat-oss/poshansip/internal/client"
	"gitlab.com/sehat-oss/poshansip/internal/token"
	"gitlab.com/sehat-oss/poshansip/internal/user"
)

// Options define options for auth service
type Options func(*Handler)

// WithAuthService assign auth service into auth grpc handler
func WithAuthService(svc auth.Service) Options {
	return func(handler *Handler) {
		handler.services.auth = svc
	}
}

// WithUserService assign user service into auth grpc handler
func WithUserService(svc user.Service) Options {
	return func(handler *Handler) {
		handler.services.user = svc
	}
}

// WithTokenService assign token service into auth grpc handler
func WithTokenService(svc token.Service) Options {
	return func(handler *Handler) {
		handler.services.token = svc
	}
}

// WithClientService assign client service into auth grpc handler
func WithClientService(svc client.Service) Options {
	return func(handler *Handler) {
		handler.services.client = svc
	}
}
