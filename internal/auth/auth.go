package auth

import (
	"context"

	"github.com/dgrijalva/jwt-go"
	userModels "gitlab.com/sehat-oss/poshansip/internal/user/models"
)

// Service defining auth service should have methods
type Service interface {
	Logout(ctx context.Context) (err error)
	EmailAuthentication(ctx context.Context, email string, password string) (user userModels.User, err error)
	RefreshTokenAuthentication(ctx context.Context, token string) (claims jwt.MapClaims, err error)
}

// Repository defining auth repository should have methods
type Repository interface {
}

const (
	PasswordGrantType     = "password"
	RefreshTokenGrantType = "refresh-token"
	ImplicitGrantType     = "implicit"
)

const (
	AuthErrorPrefix = "Error auth: %s"
)
