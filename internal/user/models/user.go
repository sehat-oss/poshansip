package models

import "time"

// User define user model data structure
type User struct {
	ID           uint64     `db:"id"`
	UserName     string     `db:"username"`
	IDCardNumber string     `db:"id_card_number"`
	Password     string     `db:"password"`
	CreatedAt    *time.Time `db:"created_at"`
	UpdatedAt    *time.Time `db:"updated_at"`
}
