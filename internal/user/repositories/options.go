package repositories

import "gitlab.com/sehat-oss/poshansip/driver/db"

// Options define functional options for user repository
type Options func(*Repository)

// WithDatabaseManager assign database manager into user repository
func WithDatabaseManager(m *db.Manager) Options {
	return func(r *Repository) {
		r.dbM = m
	}
}
