package repositories

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/sehat-oss/poshansip/driver/db"
	"gitlab.com/sehat-oss/poshansip/internal/user"
	"gitlab.com/sehat-oss/poshansip/internal/user/models"
)

// Repository define struct for user repository
type Repository struct {
	dbM       *db.Manager
	statement statement
}

type statement struct {
	poshansip poshansipStatement
}

type poshansipStatement struct {
	findByUsername *sqlx.NamedStmt
}

// New create new user repository instance
func New(opts ...Options) user.Repository {
	repo := &Repository{}

	for _, opt := range opts {
		opt(repo)
	}

	return repo
}

// FindByUsername find user account record by username
func (r *Repository) FindByUsername(ctx context.Context, username string) (user models.User, err error) {

	if r.statement.poshansip.findByUsername == nil {
		r.statement.poshansip.findByUsername, err = r.dbM.GetPoshansip().PrepareNamedContext(ctx, poshansipFindByUsername)
		if err != nil {
			return
		}
	}

	args := map[string]interface{}{
		"username": username,
	}

	err = r.statement.poshansip.findByUsername.GetContext(ctx, &user, args)

	return
}
