package services

import (
	"gitlab.com/sehat-oss/poshansip/internal/user"
)

// Options define options for auth service
type Options func(*Service)

// WithUserRepository assign user repository into auth service
func WithUserRepository(repo user.Repository) Options {
	return func(serv *Service) {
		serv.repository.user = repo
	}
}
