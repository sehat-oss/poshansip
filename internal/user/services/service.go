package services

import (
	"context"

	"gitlab.com/sehat-oss/poshansip/internal/user"
	"gitlab.com/sehat-oss/poshansip/internal/user/models"
)

// Service define service struct for user
type Service struct {
	repository repository
}

type repository struct {
	user user.Repository
}

// New create new instance of user service
func New(opts ...Options) user.Service {
	serv := new(Service)

	for _, opt := range opts {
		opt(serv)
	}

	return serv
}

// GetUserByEmail get user by email adress
func (s *Service) GetUserByEmail(ctx context.Context, email string) (user models.User, err error) {
	user, err = s.repository.user.FindByUsername(ctx, email)
	return
}
