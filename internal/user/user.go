package user

import (
	"context"

	"gitlab.com/sehat-oss/poshansip/internal/user/models"
)

// Repository define repository interface for user
type Repository interface {
	FindByUsername(ctx context.Context, username string) (user models.User, err error)
}

// Service define service interface for user
type Service interface {
	GetUserByEmail(ctx context.Context, email string) (user models.User, err error)
}
