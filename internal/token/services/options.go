package services

import (
	"gitlab.com/sehat-oss/poshansip/internal/token"
)

// Options define options for auth service
type Options func(*Service)

// WithTokenRepository assign token repository into auth service
func WithTokenRepository(repo token.Repository) Options {
	return func(serv *Service) {
		serv.repository.token = repo
	}
}
