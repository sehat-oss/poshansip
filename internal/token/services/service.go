package services

import (
	"context"
	"time"

	"gitlab.com/sehat-oss/poshansip/internal/token/models"

	"gitlab.com/sehat-oss/poshansip/internal/auth"
	"gitlab.com/sehat-oss/poshansip/internal/token"
	userModels "gitlab.com/sehat-oss/poshansip/internal/user/models"
	tokenPkg "gitlab.com/sehat-oss/poshansip/pkg/token"
)

// Service define struct for token service
type Service struct {
	repository repository
}

type repository struct {
	token token.Repository
}

// New create new token service instance
func New(opts ...Options) token.Service {
	service := new(Service)

	for _, opt := range opts {
		opt(service)
	}

	return service
}

// CheckAccessTokenOnDB check whether access token still valid
func (s *Service) CheckAccessTokenOnDB(ctx context.Context, token string, userID uint64) (isExists bool, tokenModel models.Token, err error) {
	tokenModels, err := s.repository.token.FindTokenByUserID(ctx, userID)
	if err != nil {
		return
	}

	for _, tokenModel = range tokenModels {
		if tokenModel.AccessToken == token {
			isExists = true
			return
		}
	}

	return
}

// CheckRefreshTokenOnDB check whether refresh token still valid
func (s *Service) CheckRefreshTokenOnDB(ctx context.Context, token string, userID uint64) (isExists bool, err error) {
	tokenModels, err := s.repository.token.FindTokenByUserID(ctx, userID)
	if err != nil {
		return
	}

	for _, tokenModel := range tokenModels {
		if tokenModel.RefreshToken == token {
			isExists = true
			return
		}
	}

	return
}

// GetPasswordGrantToken create token for password grant flow
func (s *Service) GetPasswordGrantToken(ctx context.Context, user userModels.User, clientID uint64) (token models.Token, err error) {

	payload := tokenPkg.Payload{
		UserID: user.ID,
	}

	refreshToken, refreshTokenExpiresAt, err := tokenPkg.GetRefreshToken(payload)
	if err != nil {
		return
	}

	accessToken, accessTokenExpiresAt, err := tokenPkg.GetNewToken(payload, refreshToken)
	if err != nil {
		return
	}

	refreshTokenExpiration := time.Unix(refreshTokenExpiresAt, 0)
	accessTokenExpiration := time.Unix(accessTokenExpiresAt, 0)

	token = models.Token{
		UserID:                user.ID,
		GrantType:             auth.PasswordGrantType,
		AccessToken:           accessToken,
		RefreshToken:          refreshToken,
		AccessTokenExpiresAt:  &accessTokenExpiration,
		RefreshTokenExpiresAt: &refreshTokenExpiration,
		CreatedAt:             time.Now(),
	}

	err = s.repository.token.SaveToken(ctx, token, clientID)

	return
}

// GetImplicitGrantToken create token for implicit grant flow
func (s *Service) GetImplicitGrantToken(ctx context.Context, user userModels.User, clientID uint64) (token models.Token, err error) {
	payload := tokenPkg.Payload{
		UserID: user.ID,
	}

	accessToken, accessTokenExpiresAt, err := tokenPkg.GetImplicitToken(payload)
	if err != nil {
		return
	}

	accessTokenExpiration := time.Unix(accessTokenExpiresAt, 0)

	token = models.Token{
		UserID:               user.ID,
		GrantType:            auth.ImplicitGrantType,
		AccessToken:          accessToken,
		AccessTokenExpiresAt: &accessTokenExpiration,
		CreatedAt:            time.Now(),
	}

	err = s.repository.token.SaveToken(ctx, token, clientID)

	return
}

// GetRefreshTokenGrantToken create token for refresh token grant flow
func (s *Service) GetRefreshTokenGrantToken(ctx context.Context, tokenStr string, userID uint64, clientID uint64) (token models.Token, err error) {
	payload := tokenPkg.Payload{
		UserID: userID,
	}

	accessToken, accessTokenExpiresAt, err := tokenPkg.GetNewToken(payload, tokenStr)
	if err != nil {
		return
	}

	accessTokenExpiration := time.Unix(accessTokenExpiresAt, 0)

	token = models.Token{
		UserID:               userID,
		GrantType:            auth.RefreshTokenGrantType,
		AccessToken:          accessToken,
		AccessTokenExpiresAt: &accessTokenExpiration,
	}

	err = s.repository.token.UpdateToken(ctx, token, clientID)

	return
}

// RevokeToken revoke token from user
func (s *Service) RevokeToken(ctx context.Context, tokenID uint64) (err error) {
	err = s.repository.token.DeleteToken(ctx, tokenID)
	return
}
