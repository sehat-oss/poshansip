package repositories

import "gitlab.com/sehat-oss/poshansip/driver/db"

// Options define functional options for token repository
type Options func(*Repository)

// WithDatabaseManager assign database manager into token repository
func WithDatabaseManager(m *db.Manager) Options {
	return func(r *Repository) {
		r.dbM = m
	}
}
