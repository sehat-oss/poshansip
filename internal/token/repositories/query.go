package repositories

const (
	poshansipFindByUserID = `SELECT * FROM tokens WHERE user_id = :user_id`
	poshansipInsert       = `INSERT INTO 
			tokens (user_id, access_token, client_id, grant_type, refresh_token, access_token_expires, 
					refresh_token_expires, created_at)
			VALUES 
				(:user_id, :access_token, :client_id, :grant_type, :refresh_token, :access_token_expires, 
					:refresh_token_expires, :created_at)
		`
	poshansipUpdate = `UPDATE tokens 
			SET
				access_token = :access_token,
				access_token_expires = :access_token_expires,
				grant_type = :grant_type
			WHERE
				user_id = :user_id
			AND
				client_id = :client_id
		`
	poshansipDelete = `DELETE FROM tokens WHERE id = :id`
)
