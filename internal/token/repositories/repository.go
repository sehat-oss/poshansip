package repositories

import (
	"context"

	"gitlab.com/sehat-oss/poshansip/internal/customerror"

	"github.com/jmoiron/sqlx"
	"gitlab.com/sehat-oss/poshansip/driver/db"
	"gitlab.com/sehat-oss/poshansip/internal/token"
	"gitlab.com/sehat-oss/poshansip/internal/token/models"
)

// Repository define struct for token repository
type Repository struct {
	dbM       *db.Manager
	statement statement
}

type statement struct {
	poshansip poshansipStatement
}

type poshansipStatement struct {
	insertToken  *sqlx.NamedStmt
	updateToken  *sqlx.NamedStmt
	deleteToken  *sqlx.NamedStmt
	findByUserID *sqlx.NamedStmt
}

// New create new user repository instance
func New(opts ...Options) token.Repository {
	repo := &Repository{}

	for _, opt := range opts {
		opt(repo)
	}

	return repo
}

// FindTokenByUserID find token by user id
func (r *Repository) FindTokenByUserID(ctx context.Context, userID uint64) (token []models.Token, err error) {

	if r.statement.poshansip.findByUserID == nil {
		r.statement.poshansip.findByUserID, err = r.dbM.GetPoshansip().PrepareNamedContext(ctx, poshansipFindByUserID)
		if err != nil {
			return
		}
	}

	args := map[string]interface{}{
		"user_id": userID,
	}

	err = r.statement.poshansip.findByUserID.SelectContext(ctx, &token, args)

	return
}

// SaveToken save token to database
func (r *Repository) SaveToken(ctx context.Context, token models.Token, clientID uint64) (err error) {

	if r.statement.poshansip.insertToken == nil {
		r.statement.poshansip.insertToken, err = r.dbM.GetPoshansip().PrepareNamedContext(ctx, poshansipInsert)
		if err != nil {
			return
		}
	}

	args := map[string]interface{}{
		"user_id":               token.UserID,
		"grant_type":            token.GrantType,
		"client_id":             clientID,
		"access_token":          token.AccessToken,
		"refresh_token":         token.RefreshToken,
		"access_token_expires":  token.AccessTokenExpiresAt,
		"refresh_token_expires": token.RefreshTokenExpiresAt,
		"created_at":            token.CreatedAt,
	}

	_, err = r.statement.poshansip.insertToken.ExecContext(ctx, args)

	return
}

// UpdateToken save token on database
func (r *Repository) UpdateToken(ctx context.Context, token models.Token, clientID uint64) (err error) {

	if r.statement.poshansip.updateToken == nil {
		r.statement.poshansip.updateToken, err = r.dbM.GetPoshansip().PrepareNamedContext(ctx, poshansipUpdate)
		if err != nil {
			return
		}
	}

	args := map[string]interface{}{
		"user_id":              token.UserID,
		"grant_type":           token.GrantType,
		"client_id":            clientID,
		"access_token":         token.AccessToken,
		"access_token_expires": token.AccessTokenExpiresAt,
	}

	result, err := r.statement.poshansip.updateToken.ExecContext(ctx, args)
	if err != nil {
		return
	}

	rowAffected, err := result.RowsAffected()
	if rowAffected == 0 {
		err = customerror.ErrAuthTokenHasBeenRevoked
	}

	return
}

// DeleteToken delete token from database
func (r *Repository) DeleteToken(ctx context.Context, id uint64) (err error) {

	if r.statement.poshansip.deleteToken == nil {
		r.statement.poshansip.deleteToken, err = r.dbM.GetPoshansip().PrepareNamedContext(ctx, poshansipDelete)
		if err != nil {
			return
		}
	}

	args := map[string]interface{}{
		"id": id,
	}

	_, err = r.statement.poshansip.deleteToken.ExecContext(ctx, args)

	return
}
