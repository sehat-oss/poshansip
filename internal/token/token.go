package token

import (
	"context"

	tokenModels "gitlab.com/sehat-oss/poshansip/internal/token/models"
	userModels "gitlab.com/sehat-oss/poshansip/internal/user/models"
)

// Repository define repository interface for token
type Repository interface {
	SaveToken(ctx context.Context, token tokenModels.Token, clientID uint64) (err error)
	UpdateToken(ctx context.Context, token tokenModels.Token, clientID uint64) (err error)
	DeleteToken(ctx context.Context, id uint64) (err error)
	FindTokenByUserID(ctx context.Context, userID uint64) (token []tokenModels.Token, err error)
}

// Service define service interface for token
type Service interface {
	CheckAccessTokenOnDB(ctx context.Context, token string, userID uint64) (isExists bool, tokenModel tokenModels.Token, err error)
	CheckRefreshTokenOnDB(ctx context.Context, token string, userID uint64) (isExists bool, err error)
	GetPasswordGrantToken(ctx context.Context, user userModels.User, clientID uint64) (token tokenModels.Token, err error)
	GetImplicitGrantToken(ctx context.Context, user userModels.User, clientID uint64) (token tokenModels.Token, err error)
	GetRefreshTokenGrantToken(ctx context.Context, tokenStr string, userID uint64, clientID uint64) (token tokenModels.Token, err error)
	RevokeToken(ctx context.Context, tokenID uint64) (err error)
}
