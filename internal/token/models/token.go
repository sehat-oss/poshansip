package models

import "time"

// Token define token model data structure
type Token struct {
	ID                    uint64     `db:"id"`
	UserID                uint64     `db:"user_id"`
	ClientID              uint64     `db:"client_id"`
	GrantType             string     `db:"grant_type"`
	AccessToken           string     `db:"access_token"`
	RefreshToken          string     `db:"refresh_token"`
	AccessTokenExpiresAt  *time.Time `db:"access_token_expires"`
	RefreshTokenExpiresAt *time.Time `db:"refresh_token_expires"`
	CreatedAt             time.Time  `db:"created_at"`
}
