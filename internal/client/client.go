package client

import (
	"context"

	"gitlab.com/sehat-oss/poshansip/internal/client/models"
)

// Service define interface for client services
type Service interface {
	GetClientByUserIDAndClientInfo(ctx context.Context, userID uint64, clientID string, clientSecret string) (client models.Client, err error)
	ValidateUserClient(ctx context.Context, userID uint64, clientID string, clientSecret string) (valid bool, dbClientID uint64, err error)
}

// Repository define interface for client repository
type Repository interface {
	GetClientByUserIDAndClientInfo(ctx context.Context, userID uint64, clientID string, clientSecret string) (clients models.Client, err error)
}
