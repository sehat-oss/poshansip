package repositories

import "gitlab.com/sehat-oss/poshansip/driver/db"

// Options define optional function for client repository
type Options func(*Repository)

// WithDatabaseManager assign database manager into client repository
func WithDatabaseManager(db *db.Manager) Options {
	return func(repo *Repository) {
		repo.dbM = db
	}
}
