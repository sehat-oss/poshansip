package repositories

const (
	poshansipGetClientByUserID = `SELECT 
			clients.* 
		FROM
			clients
		INNER JOIN 
			users_clients ON users_clients.client_id = clients.id
		WHERE 
			users_clients.user_id = :user_id
		AND
			clients.client_id = :client_id
		AND
			clients.client_secret = :client_secret
		`
)
