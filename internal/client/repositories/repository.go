package repositories

import (
	"context"

	"github.com/jmoiron/sqlx"
	"gitlab.com/sehat-oss/poshansip/driver/db"
	"gitlab.com/sehat-oss/poshansip/internal/client"
	"gitlab.com/sehat-oss/poshansip/internal/client/models"
)

// Repository define struct for client repository
type Repository struct {
	dbM       *db.Manager
	statement statement
}

type statement struct {
	poshansip poshansip
}

type poshansip struct {
	getClientByUserID *sqlx.NamedStmt
}

// New create new instance of client repository
func New(opts ...Options) client.Repository {
	repo := new(Repository)

	for _, opt := range opts {
		opt(repo)
	}

	return repo
}

// GetClientByUserIDAndClientInfo get client records by user_id, client_id and client_secret
func (r *Repository) GetClientByUserIDAndClientInfo(ctx context.Context, userID uint64, clientID string, clientSecret string) (client models.Client, err error) {
	if r.statement.poshansip.getClientByUserID == nil {
		r.statement.poshansip.getClientByUserID, err = r.dbM.GetPoshansip().PrepareNamedContext(ctx, poshansipGetClientByUserID)
		if err != nil {
			return
		}
	}

	args := map[string]interface{}{
		"user_id":       userID,
		"client_id":     clientID,
		"client_secret": clientSecret,
	}

	err = r.statement.poshansip.getClientByUserID.GetContext(ctx, &client, args)

	return
}
