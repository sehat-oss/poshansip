package services

import "gitlab.com/sehat-oss/poshansip/internal/client"

// Options define functional options for client service
type Options func(*Service)

// WithClientRepository assign client repository into client service
func WithClientRepository(repo client.Repository) Options {
	return func(serv *Service) {
		serv.repository.client = repo
	}
}
