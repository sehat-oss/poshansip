package services

import (
	"context"
	"database/sql"

	"gitlab.com/sehat-oss/poshansip/internal/client"
	"gitlab.com/sehat-oss/poshansip/internal/client/models"
)

// Service define struct for client service
type Service struct {
	repository repository
}

type repository struct {
	client client.Repository
}

// New create new instance of client service
func New(opts ...Options) client.Service {
	serv := new(Service)

	for _, opt := range opts {
		opt(serv)
	}

	return serv
}

// GetClientByUserIDAndClientInfo get client records by user_id, client_id and client_secret from repository
func (s *Service) GetClientByUserIDAndClientInfo(ctx context.Context, userID uint64, clientID string, clientSecret string) (client models.Client, err error) {
	client, err = s.repository.client.GetClientByUserIDAndClientInfo(ctx, userID, clientID, clientSecret)
	return
}

// ValidateUserClient validating user client
func (s *Service) ValidateUserClient(ctx context.Context, userID uint64, clientID string, clientSecret string) (valid bool, dbClientID uint64, err error) {
	client, err := s.GetClientByUserIDAndClientInfo(ctx, userID, clientID, clientSecret)
	if err == sql.ErrNoRows {
		err = nil
		return
	}

	if err != nil {
		return
	}

	if &client == nil {
		return
	}

	dbClientID = client.ID
	valid = true

	return
}
