package models

import "time"

// UserClient define user_client model data structure
type UserClient struct {
	ID        uint64    `db:"id"`
	ClientID  uint64    `db:"client_id"`
	UserID    uint64    `db:"user_id"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"deleted_at"`
}
