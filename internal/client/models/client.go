package models

import "time"

// Client define client model data structure
type Client struct {
	ID           uint64    `db:"id"`
	ClientID     string    `db:"client_id"`
	ClientSecret string    `db:"client_secret"`
	ClientName   string    `db:"client_name"`
	CreatedAt    time.Time `db:"created_at"`
	UpdatedAt    time.Time `db:"updated_at"`
}
