package customerror

const (
	// LanguageID indonesian type for error message
	LanguageID = `ID`
	// LanguageEN english type for error message
	LanguageEN = `EN`
)

// CustomError is a trivial implementation of error.
type err map[string]string

func (e *err) Error() string {
	return (*e)[LanguageEN]
}
