package customerror

var (
	// ErrAuthNoGrantType error message when grant type not provided
	ErrAuthNoGrantType = &err{
		LanguageEN: "Grant type cannot be empty",
		LanguageID: "Grant type tidak boleh kosong",
	}

	// ErrAuthNotSupportedGrantType error message when provided grant type not supported
	ErrAuthNotSupportedGrantType = &err{
		LanguageEN: "Unsuported grant type",
		LanguageID: "Grant type tidak di-support",
	}

	// ErrAuthBadUsernamePassword error message when user given wrong username or/and password
	ErrAuthBadUsernamePassword = &err{
		LanguageEN: "Wrong username or password",
		LanguageID: "Username dan/atau Password Anda salah",
	}

	// ErrAuthInvalidToken error message when token invalid or exipred
	ErrAuthInvalidToken = &err{
		LanguageEN: "Token is invalid",
		LanguageID: "Token tidak valid",
	}

	// ErrAuthExtractTokenClaims error message when cannot extract token claims
	ErrAuthExtractTokenClaims = &err{
		LanguageEN: "Failed on reading token claims",
		LanguageID: "Gagal membaca klaim token",
	}

	// ErrAuthTokenHasBeenRevoked error message when token not found on database
	ErrAuthTokenHasBeenRevoked = &err{
		LanguageEN: "Token has been revoked",
		LanguageID: "Token telah dihapus",
	}

	// ErrNoTokenProvided error message when access token not provided
	ErrNoTokenProvided = &err{
		LanguageEN: "Token cannot be empty",
		LanguageID: "Token tidak boleh kosong",
	}

	// ErrNoClientIDProvided error message when client_id not provided
	ErrNoClientIDProvided = &err{
		LanguageEN: "client_id cannot be empty",
		LanguageID: "client_id tidak boleh kosong",
	}

	// ErrNoClientSecretProvided error message when client_secret not provided
	ErrNoClientSecretProvided = &err{
		LanguageEN: "client_secret cannot be empty",
		LanguageID: "client_secret tidak boleh kosong",
	}

	// ErrWrongClientIDOrClientSecret error message for wrong client_id or client_secret
	ErrWrongClientIDOrClientSecret = &err{
		LanguageEN: "Wrong client_id or client_secret",
		LanguageID: "client_id atau client_secret salah",
	}
)
